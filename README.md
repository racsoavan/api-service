# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
ruby '2.5.7'
* Rails version
ruby 'rails', '~> 6.0.3', '>= 6.0.3.3'
* System dependencies
mongodb
redis
ubuntu
* Configuration
Para cargar la configuracion, se requieren cargar las variables de entorno que estan .env.example con los valores reales para arrancar el servicio
* Database creation
N/A
* Database initialization
Solo se requiere la configuración de las variables de entorno para conectarse a mongodb
## How to run the test suite
Dentro de la carpeta del proyecto
para modelos:
rails test test/models
para controladores:
rails test test/controllers

General o todos:
rails test
## endpoints
Para cargar una nueva tarjeta
```
    path: /api/v1/services
    methods: POST
    params: {"name":"", "card_number":"", "cvc":"", "date_expiration":""}
    Ejemplo: 
        curl --header "Content-Type: application/json" \
        --request POST \
        --data '{"name":"Nombre tarjtetabiente", "card_number":"numero tarjeta", "cvc":"123", "date_expiration":"02/25"}' \
        http://localhost:3000/api/v1/services
    responde:
        error: {errors: []} //indica si la tarjeta no se pudo procesar por ser invalida o si faltan datos para ser almacenada
        success: {token: @TOKEN_GENERADO}
```
Para realizar un cargo
```
    path: /api/v1/services/redeem
    methods: POST
    params: {"amount":"200.16", "items":{"item1": "producto 1","item2": "producto 2"}}
    authentication: Authorization: Bearer @TOKEN_GENERADO
    Ejemplo: 
        curl --header "Content-Type: application/json" \
        --request POST \
        -H "Accept: application/json" \
        -H "Authorization: Bearer @TOKEN_GENERADO" \
        --data '{"amount":"200.16", "items":{"item1": "producto 1","item2": "producto 2"}}' \
        http://localhost:3000/api/v1/services/redeem
    responde:
        error: {errors: []} //indica si el token aun es valido, tiempo de vida 10min o si faltan datos para ser el cargo
        success: {data: info_cargo} //retorna la información del nuevo cargo
```
Mostrar todos los cargos durante los 10 minutos
```
    path: /api/v1/services/all_redeems
    methods: GET
    params: {"amount":"200.16", "items":{"item1": "producto 1","item2": "producto 2"}}
    authentication: Authorization: Bearer @TOKEN_GENERADO
    Ejemplo: 
        curl --header "Content-Type: application/json" \
        --request GET \
        -H "Accept: application/json" \
        -H "Authorization: Bearer @TOKEN_GENERADO" \
        http://localhost:3000/api/v1/services/all_redeems
    responde:
        error: {errors: []} //indica si el token aun es valido, tiempo de vida 10min 
        success: {data: redeems} //retorna todos los cargos 
```


## Nota: 
    Rama de without_redis
    Contiene el mismo procedimiento sin redis, es decir la información sencible esta dentro del token, al pasar el token los datos de tarjeta de credito, cvc y fecha de expiración, el token tiene un tiempo de 10 minutos asi que despues de ese tiempo ya no se puede utilizar.