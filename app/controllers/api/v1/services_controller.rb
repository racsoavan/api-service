class Api::V1::ServicesController < ApplicationController
    require 'credit_card_bins'
    before_action :fill_info, only: :create
    before_action :authorize_request, only: [:redeem,:all_redeems]
    def create
        card = Card.new(validate_params)
        if card.save
          render json: {token: card.secure_info({card_id: card.id, card_number: @card_number}, @info_encrypt)}, status: :created
        else
          @info_encrypt = nil
          render json: card.errors, status: :unprocessable_entity
        end
    end
    def redeem
        card = Card.find(@info_card["card_id"])
        info_to_make_process = card.get_info_card #Obtiene la información desencriptada para realizar las operaciones
        redeem = card.redeems.build(validate_redeem)
        if redeem.save
          render json: {message: "Success", data:redeem}, status: :created
        else
            render json: redeem.errors, status: :unprocessable_entity
        end
    end
    def all_redeems
        card = Card.find(@info_card["card_id"])
        redeems = card.redeems
        render json: {message: "Success", data:redeems}, status: :created
    end
    private
    def validate_redeem
        params.require(:service).permit(:amount, items: {})
    end
    def validate_params
        params.require(:service).permit(:name, :date_expiration, :bin, :last_numbers, :type_card, :card_brand)
    end
    def fill_info
        begin
            card_number = params[:service][:card_number].to_s
            info_card = CreditCardBin.new(card_number)
            params[:service][:bin] = info_card.bin
            params[:service][:last_numbers] = card_number[-4..-1].strip
            params[:service][:type_card] = info_card.type
            params[:service][:card_brand] = info_card.brand
            @card_number = params[:service][:card_number]
            @info_encrypt = {cvc:params[:service][:cvc], date_expiration: params[:service][:date_expiration]}
            params[:service].delete :card_number
            params[:service].delete :cvc
        rescue StandardError => e
            #p ":::::::::::::::::::::::: Error Card"
            print e
            error = true
        end
        render json: "Card cannot be processed", status: :unprocessable_entity if error
    end
end
