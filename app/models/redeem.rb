class Redeem
  include Mongoid::Document
  
  belongs_to :card
  field :items, type: Object
  field :amount, type: Float

  include Mongoid::Timestamps

  validates :amount, :items, presence: true
  validates :items, length: { minimum: 1 }
end
