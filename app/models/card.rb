class Card
  
  include Mongoid::Document
  field :name, type: String
  field :bin, type: Integer
  field :last_numbers, type: String
  field :date_expiration, type: String
  field :type_card, type: String
  field :card_brand, type: String
  include Mongoid::Timestamps
  
  validates :name, :bin, :last_numbers, :date_expiration, :card_brand, :type_card, presence: true
  has_many :redeems

  def secure_info(info,data)
    encrypt = get_key.encrypt_and_sign(data)
    key_card = "card:#{info[:card_id]}"
    $redis.set(key_card,encrypt)
    $redis.expire(key_card,10.minute.to_i)
    return JsonWebToken.encode(info)
  end
  def get_info_card
    data = $redis.get("card:#{self.id}")
    info = get_key.decrypt_and_verify(data)
  end
  def get_key
    len   = ActiveSupport::MessageEncryptor.key_len
    salt  = '\xDE7\x90\xF3d\x03\x057\x05C\xDEz\x84\xE2\xD4\xE6\xC5\xEF\x86\xE5\x0E7\x06\"fO\xEC\x8Ax\xCC\xABd'
    key   = ActiveSupport::KeyGenerator.new(ENV["KEY_ENCRYPT"]).generate_key(salt, len) 
    return ActiveSupport::MessageEncryptor.new(key)    
  end
end
