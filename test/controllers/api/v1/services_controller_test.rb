require 'test_helper'

class Api::V1::ServicesControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  test "Create Card" do
    @token = nil
    post '/api/v1/services', params:{service:{"name":"Oscar Nava", "card_number":"4599830100592012", "cvc":"123", "date_expiration":"02/25"}}
    json_response = JSON.parse(response.body)
    @token = json_response["token"]
    assert_response :created
    
    post '/api/v1/services/redeem', params:{service: {"amount":"200.16", "items":{"item1": "producto Test 1","item2": "producto Test 2"}}},headers: { 'Authorization': "Bearer #{@token}" }
    assert_response :created

    get '/api/v1/services/all_redeems', params:{},headers: { 'Authorization': "Bearer #{@token}" }
    assert_response :created
  end
end
