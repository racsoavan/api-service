require 'test_helper'

class CardTest < ActiveSupport::TestCase
  def setup
    @card = Card.new(name: 'test Name', bin: 254852, last_numbers: '1257', date_expiration: '05/25', type_card: :DEBIT, card_brand: :MASTERCARD)
  end
  test "New Card" do
    assert @card.valid?
  end
  test "Name should be present" do
    #@card.name = nil
    @card.valid?
    assert_empty @card.errors[:name], 'no validation error for name present'
  end
  test "Bin should be present" do
    #@card.bin = nil
    @card.valid?
    assert_empty @card.errors[:bin], 'no validation error for Bin present'
  end
  test "Last Numbers should be present" do
    #@card.last_numbers = nil
    @card.valid?
    assert_empty @card.errors[:last_numbers], 'no validation error for Last Numbers present'
  end
  test "Date Expiration should be present" do
    #@card.date_expiration = nil
    @card.valid?
    assert_empty @card.errors[:date_expiration], 'no validation error for Date Expiration present'
  end
  test "Type Card should be present" do
    #@card.type_card = nil
    @card.valid?
    assert_empty @card.errors[:type_card], 'no validation error for Type Card present'
  end
  test "Card Brand should be present" do
    #@card.card_brand = nil
    @card.valid?
    assert_empty @card.errors[:card_brand], 'no validation error for Card Brand present'
  end
end
