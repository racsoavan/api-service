Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :services, only: [:create] do
        collection do
          post :redeem
          get  :all_redeems
        end        
      end
    end
  end
end
